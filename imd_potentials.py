#!/usr/bin/env python
#import subprocess
#import argparse
#import re
#import os
import math

class eam:

    def __init__(self,pair,embed,den):
        self.pair = pair
        self.embed = embed
        self.den = den
        self.format = ''
        self.cutoff = 0
        self.types = 0

        # Read in pair part of potential:
        f = open(self.pair, 'r+')
        for line in f:
            if line.startswith('#F')  == True:
                tmp = line.strip().split()
                self.format = int(tmp[1])
                self.types = int(tmp[2])
            if self.format == 2 and line.startswith('#') == False and line.startswith(' ') == False:
                tmp = line.strip().split()
                self.cutoff = math.sqrt(float(tmp[1]))
                break


#----------------------------------------------------------------------------------------------
#files = find('/home/gmp/moeller/data_lxserver/share/POTENTIALS/W_FS_AT','.*[.](?!pdf$|c$).*$')
#grepper = grep(lambda line: "#F 2" in line)

#for f in files:
#    fname = file(f)
#    matched = grepper(fname)
#    if len(list(matched)) > 0:
#        print f

