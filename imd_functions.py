#!/usr/bin/env python
import subprocess
#import argparse
import re
import os



def write_paramfile_header(param,pair,embed,den,config,outfiles,pbc,ntypes,ttypes):
   f = open(param, 'w')
   f.write('core_potential_file                '+pair+'\n')
   f.write('embedding_energy_file              '+embed+'\n')
   f.write('atomic_e-density_file              '+den+'\n')
   f.write('coordname                          '+config+'\n')
   f.write('outfiles                           '+outfiles+'\n')
   f.write('pbc_dirs                           '+pbc+'\n')
   f.write('box_from_header                    1\n')
   f.write('nbl_size                           1.2\n')
   f.write('nbl_margin                         0.44\n')
   f.write('ntypes                             '+ntypes+'\n')
   f.write('total_types                        '+ttypes+'\n')
   f.close()

def write_itr(itr,chkpt,startstep,box):
   f = open(itr, 'w')
   f.write('# checkpoint      '+str(chkpt)+'\n')
   f.write('startstep         '+str(startstep)+'\n')
   f.write('box_x           '+str.format("{0:" ">12.6f}",box[0][0])+' '+str.format("{0:" ">12.6f}",box[0][1])+' '+str.format("{0:" ">12.6f}",box[0][2])+'\n')
   f.write('box_y           '+str.format("{0:" ">12.6f}",box[1][0])+' '+str.format("{0:" ">12.6f}",box[1][1])+' '+str.format("{0:" ">12.6f}",box[1][2])+'\n')
   f.write('box_z           '+str.format("{0:" ">12.6f}",box[2][0])+' '+str.format("{0:" ">12.6f}",box[2][1])+' '+str.format("{0:" ">12.6f}",box[2][2])+'\n')
   f.close()

def write_paramfile_box(param,box):
   f = open(param, 'a')
   f.write('box_x                            '+str.format("{0:" ">12.6f}",box[0][0])+' '+str.format("{0:" ">12.6f}",box[0][1])+' '+str.format("{0:" ">12.6f}",box[0][2])+'\n')
   f.write('box_y                            '+str.format("{0:" ">12.6f}",box[1][0])+' '+str.format("{0:" ">12.6f}",box[1][1])+' '+str.format("{0:" ">12.6f}",box[1][2])+'\n')
   f.write('box_z                            '+str.format("{0:" ">12.6f}",box[2][0])+' '+str.format("{0:" ">12.6f}",box[2][1])+' '+str.format("{0:" ">12.6f}",box[2][2])+'\n')
   f.close()

def write_paramfile_press(param,maxsteps):
   f = open(param, 'a')
   f.write(''+'\n')
   f.write('simulation                         1\n')
   f.write('ensemble                           mik\n')
   f.write('maxsteps                           '+str(maxsteps)+'\n') 
   f.write('eng_int                            1\n')       
   f.write('press_int                          1\n')     
   f.write('timestep                           0.001\n')     
#   f.write('checkpt_int                        INT\n')
   f.close()

def write_header_stress(filename,box):
   f = open(filename, 'w')
   f.write('#F A 1 1 1 3 0 6 \n')
   f.write('#C number type mass x y z S_xx S_yy S_zz S_yz S_zx S_xy\n')
   f.write('#X           '+str.format("{0:" ">12.6f}",box[0][0])+' '+str.format("{0:" ">12.6f}",box[0][1])+' '+str.format("{0:" ">12.6f}",box[0][2])+'\n')
   f.write('#Y           '+str.format("{0:" ">12.6f}",box[1][0])+' '+str.format("{0:" ">12.6f}",box[1][1])+' '+str.format("{0:" ">12.6f}",box[1][2])+'\n')
   f.write('#Z           '+str.format("{0:" ">12.6f}",box[2][0])+' '+str.format("{0:" ">12.6f}",box[2][1])+' '+str.format("{0:" ">12.6f}",box[2][2])+'\n')
   f.write('##\n')
   f.write('##\n')
   f.write('#E\n')
   f.close()

def print_atom(atom):
#   print "%d %d %12.6f %12.6f %12.6f %12.6f" % (for a in atom[0:5]: print a)
   outstr = ''
   for a in atom[0:2]: 
     newstr = "%10d " % a
     outstr = outstr + newstr
   for a in atom[2:6]:
     newstr = "%12.6f " % a
     outstr = outstr + newstr
   for a in atom[6][:]:
     newstr = "%14.8f " % a
     outstr = outstr + newstr
   return outstr

def write_header(header):
   outstr = ''
   for line in header:
     newstr = "%s" % line
     outstr = outstr + newstr
   return outstr

