#!/usr/bin/env python
import subprocess
import argparse
import os
import re
from bash_functions import grep as grep
from bash_functions import find as find
import imd_functions
import imd_potentials
import imd_config

#from imd_potentials import main as pot

#Create variables out of shell commands
#Note triple quotes can embed Bash

#You could add another bash command here
#HOLDING_SPOT="""fake_command"""

#Determine hostname
HOSTNAME="""hostname"""
HOME="""echo $HOME"""

#Determines IP Address
#IPADDR = """
#/sbin/ifconfig -a | awk '/(cast)/ { print $2 }' | cut -d':' -f2 | head -1
#"""

def whichIMD(hostname):
    if 'ww1' in hostname:
        IMD = 'imd_eam4point_fire_fnorm_homdef_stress_nbl_mono_hpo'
        return IMD

#This function takes Bash commands and returns them
def runBash(cmd):
    p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
    out = p.stdout.read().strip()
    return out  #This is the stdout from the shell command

VERBOSE=False
def report(output,cmdtype="UNIX COMMAND:"):
   #Notice the global statement allows input from outside of function
   if VERBOSE:
       print "%s: %s" % (cmdtype, output)
   else:
       print output


#Function to control option parsing in Python
def controller():
    global VERBOSE,pot,c,pbc
    #Create instance of OptionParser Module, included in Standard Library
    p = argparse.ArgumentParser(description='A python wrapper for imd_stress',
                                            prog='imd_stress.py',
                                            usage= '%(prog)s [options]')
    p.add_argument('-p','--potential',nargs=4,help='EAM potential files',required=True,metavar=('NAME','PAIR','EMBED','DENSITY'))
    p.add_argument('-c','--config',help='Configuration of which the stresses should be calculated',required=True)
    p.add_argument('--pbc',nargs=3,help='Boundary conditions (0:free|1:periodic)',type=int,default=[0,0,0],metavar=('X','Y','Z'))
    p.add_argument('--cutoff',nargs=1,help='Cutoff radius for CNA (fcc: between 1st and 2nd nn, bcc: between 2nd and 3rd nn)',type=float,default=5.0)
#    p.add_argument('--ip','-i', action="store_true", help='gets current IP Address')
#    p.add_argument('--usage', '-u', action="store_true", help='gets disk usage of homedir')
#    p.add_argument('--verbose', '-v',
#                action = 'store_true',
#                help='prints verbosely',
#                default=False)

    #Option Handling passes correct parameter to runBash
    args = p.parse_args()
    if args.pbc:
        pbc = str(args.pbc[0])+' '+str(args.pbc[1])+' '+str(args.pbc[2])
    if args.potential:
        potname = args.potential[0]
        potfolders = [homedir+'/data_lxserver/share/POTENTIALS/'+potname,homedir+'/POTENTIALS/'+potname]
        for potfolder in potfolders:
#            print 'Searching for potential files in '+potfolder
            pair = potfolder+'/'+args.potential[1]
            embed = potfolder+'/'+args.potential[2]
            den = potfolder+'/'+args.potential[3]
            if os.path.isfile(pair) and os.path.isfile(embed) and os.path.isfile(den):
                print 'Found potential files in '+potfolder
                pot = imd_potentials.eam(pair,embed,den)
                break
            else:
               pair = ''
               embed = ''
               den = ''
        if pair == '' and embed == '' and den == '':
            print 'ERROR! No potential files found.'
            quit()
    if args.config:
        print 'Reading in configuration '+args.config
        c = imd_config.config(args.config)

#    imd_functions.write_paramfile_press('test.param',pbcstr)
#        value = runBash(IPADDR)
#        report(value,"IPADDR")
#    elif args.usage:
#        value = runBash(HOMEDIR_USAGE)
#        report(value, "HOMEDIR_USAGE")
#    else:
#       p.print_help()

def main():
    global homedir
    homedir = os.path.expanduser("~")
    controller()
    hostname = runBash(HOSTNAME)
    IMDCMD = whichIMD(hostname)
    maxsteps=0
    name=os.path.splitext(c.filename)[0]
    pparam=name+'.pparam'
    sparam=name+'.sparam'
    imd_functions.write_paramfile_header(param=pparam,
                                         pair=pot.pair,
                                         embed=pot.embed,
                                         den=pot.den,
                                         config=c.filename,
                                         outfiles=name,
                                         pbc=pbc,
                                         ntypes=str(pot.types),
                                         ttypes=str(c.max_type+1)
                                        )

    imd_functions.write_paramfile_press(pparam,maxsteps)

    print 'Running pressure calculation based on configuration file'
    runBash('nice '+IMDCMD+' -p '+pparam+'>'+pparam+'.out')

    if c.type == 'dynamic':
        tmp = c.filename.strip().split('.')
        chkpt = tmp[1]
    else:
        chkpt = 0

    pressfile = name+str.format(".{0:0>5d}",maxsteps)

    imd_functions.write_paramfile_header(param=sparam,
                                         pair=pot.pair,
                                         embed=pot.embed,
                                         den=pot.den,
                                         config=pressfile,
                                         outfiles=pressfile,
                                         pbc=pbc,
                                         ntypes=str(pot.types),
                                         ttypes=str(c.max_type+1)
                                        )
    imd_functions.write_paramfile_box(sparam,c.box)

    print 'Running stress calculation based on pressure file'
    runBash('nice imd_stress -e 5.0 -p '+sparam+'>'+sparam+'.out')
    stressfile = pressfile + '.4294967295.stress' 
    nstressfile = name+'.stress'

    imd_functions.write_header_stress(nstressfile,c.box)
    runBash('cat '+stressfile+' >>'+nstressfile)
    runBash('rm -rf '+stressfile)

#This idiom means the below code only runs when executed from command line
if __name__ == '__main__':
    main()

