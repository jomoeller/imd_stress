#!/usr/bin/env python
#import subprocess
#import argparse
#import re
#import os
#import numpy as np

def isfloat(value):
  try:
    float(value)
    return True
  except ValueError:
    return False

class config:

    def __init__(self,filename):
#        self.name = name
        self.type = ''
        self.filename = filename
        self.atoms = []
        self.box = []
        self.maxval = [0.0,0.0,0.0] 
        self.minval = [9999.0,9999.0,9999.0] 
        self.types= [] 
        self.nr_aux = 0
        self.aux_prop = []
	self.header = []

        # Read in the config:
        f = open(self.filename, 'r+')
        for line in f:
            if line.startswith('#')  == True:
                self.header.append(line)
            if line.startswith('##')  == True:
                ms = ['mik','fire','glok','cg']
                md = ['nve','nvt','npt']
                if any(ensemble in line for ensemble in ms) and filename.endswith('.ss') == True:
                    self.type = 'static'
                if any(ensemble in line for ensemble in md) and filename.endswith('.chkpt') == True:
                    self.type = 'dynamic'
            if line.startswith('#X')  == True or line.startswith('#Y') == True or line.startswith('#Z') == True:
                tmp = line.strip().split(' ')
                tmp = [i for i in tmp if isfloat(i)==True]
                x = float(tmp[0])
                y = float(tmp[1])
                z = float(tmp[2])
		self.box.append([x,y,z])
            if line.startswith('#F')  == True:
                tmp = line.strip().split(' ')
                tmp = [i for i in tmp if isfloat(i)==True]
                for i in range(4,len(tmp)):
                  self.nr_aux = self.nr_aux + int(tmp[i])
            if line.startswith('#C')  == True:
                tmp = line.strip().split(' ')
                if self.nr_aux > 0:
                  for i in range(7,len(tmp)):
                    self.aux_prop.append(tmp[i])
            if line.startswith('#') == False:
                tmp = line.strip().split()
                nr = int(tmp[0])
		atomtype = int(float(tmp[1]))
                mass = float(tmp[2])
                x = float(tmp[3])
                y = float(tmp[4])
                z = float(tmp[5])
                aux = []
                if self.nr_aux > 0:
                  for i in range(6,len(tmp)):
                    aux.append(float(tmp[i]))
                self.atoms.append([nr,atomtype,mass,x,y,z,aux[:]])

        self.nr_atoms = len(self.atoms)
        self.maxval[0] = max(a[3] for a in self.atoms)
        self.maxval[1] = max(a[4] for a in self.atoms)
        self.maxval[2] = max(a[5] for a in self.atoms)
        self.minval[0] = min(a[3] for a in self.atoms)
        self.minval[1] = min(a[4] for a in self.atoms)
        self.minval[2] = min(a[5] for a in self.atoms)
        self.max_type = max(a[1] for a in self.atoms)
        for t in range(0,self.max_type+1):
            self.types.append([a[1] for a in self.atoms].count(t))
        

    def __repr__(self):
        print self.box[:][:]
        print self.minval[:] 
        print self.maxval[:] 
        return "---" 

    def __str__(self):
        print self.box[:][:]
        print self.minval[:] 
        print self.maxval[:] 
        return "---" 
